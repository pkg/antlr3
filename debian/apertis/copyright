Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2009, Jim Idle
 2005-2010, Terence Parr
License: BSD-3-clause

Files: antlr-ant/*
Copyright: 2000-2004, The Apache Software Foundation
License: Apache-2.0

Files: debian/*
Copyright: 2008, John Leuner <jewel@debian.org>
License: BSD-3-clause

Files: gunit/*
Copyright: 2007, 2008, Leon, Jen-Yuan Su
License: BSD-3-clause

Files: gunit/src/*
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: gunit/src/main/antlr3/org/antlr/gunit/gUnit.g
Copyright: 2007, 2008, Leon Jen-Yuan Su
License: BSD-3-clause

Files: gunit/src/main/antlr3/org/antlr/gunit/swingui/parsers/StGUnit.g
Copyright: 2007, 2008, Leon Jen-Yuan Su
License: BSD-3-clause

Files: gunit/src/main/java/*
Copyright: 2007, Kenny MacDermid
License: BSD-3-clause

Files: gunit/src/main/java/org/antlr/gunit/GrammarInfo.java
 gunit/src/main/java/org/antlr/gunit/gUnitBaseTest.java
 gunit/src/main/java/org/antlr/gunit/gUnitTestInput.java
Copyright: 2007, 2008, Leon, Jen-Yuan Su
License: BSD-3-clause

Files: gunit/src/main/java/org/antlr/gunit/ITestCase.java
 gunit/src/main/java/org/antlr/gunit/ITestSuite.java
Copyright: 2009, Shaoting Cai
License: BSD-3-clause

Files: gunit/src/main/java/org/antlr/gunit/Interp.java
 gunit/src/main/java/org/antlr/gunit/JUnitCodeGen.java
 gunit/src/main/java/org/antlr/gunit/gUnitExecutor.java
 gunit/src/main/java/org/antlr/gunit/gUnitTestSuite.java
Copyright: 2007, 2008, Leon Jen-Yuan Su
License: BSD-3-clause

Files: gunit/src/main/java/org/antlr/gunit/swingui/*
Copyright: 2009, Shaoting Cai
License: BSD-3-clause

Files: gunit/src/main/resources/*
Copyright: 2007, 2008, Leon, Jen-Yuan Su
License: BSD-3-clause

Files: runtime/*
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: runtime/Java/src/main/java/org/antlr/runtime/tree/PositionTrackingStream.java
Copyright: 2011, 2012, Terence Parr
 2011, 2012, Sam Harwell
License: BSD-3-clause

Files: tool/*
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: tool/src/*
Copyright: 2011, Sam Harwell
 2005-2011, Terence Parr
License: BSD-3-clause

Files: tool/src/main/antlr3/org/antlr/grammar/v3/ANTLRv3.g
 tool/src/main/antlr3/org/antlr/grammar/v3/ANTLRv3Tree.g
 tool/src/main/antlr3/org/antlr/grammar/v3/ActionAnalysis.g
 tool/src/main/antlr3/org/antlr/grammar/v3/ActionTranslator.g
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: tool/src/main/antlr3/org/antlr/grammar/v3/CodeGenTreeWalker.g
 tool/src/main/antlr3/org/antlr/grammar/v3/LeftRecursiveRuleWalker.g
Copyright: 2011, 2012, Terence Parr
 2011, 2012, Sam Harwell
License: BSD-3-clause

Files: tool/src/main/java/*
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: tool/src/main/java/org/antlr/codegen/CSharp3Target.java
Copyright: 2008-2010, Sam Harwell, Pixel Mine, Inc.
 2005-2008, Terence Parr
License: BSD-3-clause

Files: tool/src/main/java/org/antlr/codegen/ObjCTarget.java
Copyright: 2010, Terence Parr and Alan Condit
 2006, Kay Roepke (Objective-C runtime)
License: BSD-3-clause

Files: tool/src/main/java/org/antlr/codegen/RubyTarget.java
Copyright: 2010, Kyle Yetter
License: BSD-3-clause

Files: tool/src/main/java/org/antlr/tool/ToolSTGroupFile.java
Copyright: 2011, 2012, Terence Parr
 2011, 2012, Sam Harwell
License: BSD-3-clause

Files: tool/src/main/resources/*
Copyright: 2011, Terence Parr
 2011, Sam Harwell, Tunnel Vision Laboratories, LLC.
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/*
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/C/*
Copyright: 2005-2009, Jim Idle, Temporal Wave LLC
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/CSharp2/*
Copyright: 2011, Terence Parr
 2011, Sam Harwell
 2007, 2008, Johannes Luber
 2005-2007, Kunle Odutola
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/CSharp2/ASTDbg.stg
Copyright: 2007, 2008, Johannes Luber
 2005-2007, Kunle Odutola
 2005, Terence Parr
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/CSharp3/*
Copyright: 2011, Terence Parr
 2011, Sam Harwell, Pixel Mine, Inc.
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/CSharp3/ASTDbg.stg
 tool/src/main/resources/org/antlr/codegen/templates/CSharp3/Dbg.stg
Copyright: 2008-2010, Sam Harwell, Pixel Mine, Inc.
 2005-2008, Terence Parr
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/Cpp/*
Copyright: 2005-2009, Gokulakannan Somasundaram
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/Delphi/*
Copyright: 2008, Erik van Bilsen
 2007, 2008, Johannes Luber
 2005-2007, Kunle Odutola
 2005, 2006, Terence Parr
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/ObjC/*
Copyright: 2006, 2007, Kay Roepke 2010, Alan Condit
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/ObjC/ST.stg
 tool/src/main/resources/org/antlr/codegen/templates/ObjC/ST4ObjC.stg
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/Perl5/*
Copyright: 2007, 2008, Ronald Blaschke
 2005, 2006, Terence Parr
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/codegen/templates/Scala/*
Copyright: 2010, Matthew Lloyd
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/tool/templates/messages/*
Copyright: 2006, Kay Roepke
License: BSD-3-clause

Files: tool/src/main/resources/org/antlr/tool/templates/messages/languages/*
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: tool/src/test/*
Copyright: 2005-2013, Terence Parr
License: BSD-3-clause

Files: antlr-ant/main/antlr3-task/antlr3-task.doc
Copyright: 2003-2014, Terence Parr <parrt@antlr.org>
License: BSD-3-clause
